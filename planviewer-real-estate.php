<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 13-6-18
 * Time: 12:27
 */
/*
Plugin Name:  Planviewer Real Estate Plugin
Plugin URI:   https://www.planviewer.nl/
Description:  Een demoplugin om huizen met adres te tonen
Version:      0.0.1
Author:       planviewer.nl
Author URI:   https://www.planviewer.nl/
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  pvre
*/
defined( 'ABSPATH' ) or die( '' );

function create_object_post_type() {
    register_post_type( 'object',
        array(
            'labels' => array(
                'name' => __( 'Objecten' ),
                'singular_name' => __( 'Object' )
            ),
            'public' => true,
            'show_in_menu' => true,
            'has_archive' => true,
            'menu_icon'   => 'dashicons-building',
            'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
            'capability_type' => 'post',
            'show_ui' => true,
            'show_in_nav_menus' => true,
        )
    );
}
add_action( 'init', 'create_object_post_type' );


function addCustomFields(){
    if( function_exists( 'x_add_metadata_field' ) && function_exists( 'x_add_metadata_group' ) ) {
        $args = array(
            'label' => 'Objectgegevens',
            'context' => 'side',
            'autosave' => true
        );

        x_add_metadata_group( 'objectgegevens', 'object', $args );

        x_add_metadata_field( 'straatnaam', 'object', array('field_type' => 'text', 'group' => 'objectgegevens', 'label' => 'Straatnaam') );
        x_add_metadata_field( 'huisnummer', 'object', array('field_type' => 'text', 'group' => 'objectgegevens', 'label' => 'Huisnummer') );
        x_add_metadata_field( 'toevoeging', 'object', array('field_type' => 'text', 'group' => 'objectgegevens', 'label' => 'Toevoeging') );
        x_add_metadata_field( 'postcode', 'object', array('field_type' => 'text', 'group' => 'objectgegevens', 'label' => 'Postcode') );
        x_add_metadata_field( 'Plaatsnaam', 'object', array('field_type' => 'text', 'group' => 'objectgegevens', 'label' => 'Plaatsnaam') );
    }
}
add_action('admin_menu', 'addCustomFields');


